


	window.onload = function () {

	var parallaxBox = document.getElementById ( 'box' );
	var c1left = document.getElementById ( 'top-par-1' ).offsetLeft,
	c1top = document.getElementById ( 'top-par-1' ).offsetTop,
	c2left = document.getElementById ( 'top-par-2' ).offsetLeft,
	c2top = document.getElementById ( 'top-par-2' ).offsetTop,
	c3left = document.getElementById ( 'top-par-3' ).offsetLeft,
	c3top = document.getElementById ( 'top-par-3' ).offsetTop,
	c4left = document.getElementById ( 'top-par-4' ).offsetLeft,
	c4top = document.getElementById ( 'top-par-4' ).offsetTop,
	c5left = document.getElementById ( 'top-par-5' ).offsetLeft,
	c5top = document.getElementById ( 'top-par-5' ).offsetTop,
	c6left = document.getElementById ( 'top-par-6' ).offsetLeft,
	c6top = document.getElementById ( 'top-par-6' ).offsetTop,
	c7left = document.getElementById ( 'top-par-7' ).offsetLeft,
	c7top = document.getElementById ( 'top-par-7' ).offsetTop,
	c8left = document.getElementById ( 'top-par-8' ).offsetLeft,
	c8top = document.getElementById ( 'top-par-8' ).offsetTop,
	 c9left = document.getElementById ( 'top-par-9' ).offsetLeft,
	c9top = document.getElementById ( 'top-par-9' ).offsetTop,
	c10left = document.getElementById ( 'top-par-10' ).offsetLeft,
	c10top = document.getElementById ( 'top-par-10' ).offsetTop,
	c11left = document.getElementById ( 'top-par-11' ).offsetLeft,
	c11top = document.getElementById ( 'top-par-11' ).offsetTop,
	c12left = document.getElementById ( 'top-par-12' ).offsetLeft,
	c12top = document.getElementById ( 'top-par-12' ).offsetTop,
	c13left = document.getElementById ( 'top-par-13' ).offsetLeft,
	c13top = document.getElementById ( 'top-par-13' ).offsetTop,
	c14top = document.getElementById ( 'top-par-13' ).offsetTop;
	c14left = document.getElementById ( 'top-par-14' ).offsetLeft,
	c14top = document.getElementById ( 'top-par-14' ).offsetTop;	
	c15left = document.getElementById ( 'top-par-15' ).offsetLeft,
	c15top = document.getElementById ( 'top-par-15' ).offsetTop;
	parallaxBox.onmousemove = function ( event ) {
		event = event || window.event;
		var x = event.clientX - parallaxBox.offsetLeft,
		y = event.clientY - parallaxBox.offsetTop;
		
		mouseParallax ( 'top-par-1', c1left, c1top, x, y, 12 );
		mouseParallax ( 'top-par-2', c2left, c2top, x, y, 11 );
		mouseParallax ( 'top-par-3', c3left, c3top, x, y, 14 );
		mouseParallax ( 'top-par-4', c4left, c4top, x, y, 17 );
		mouseParallax ( 'top-par-5', c5left, c5top, x, y, 20 );
		mouseParallax ( 'top-par-6', c6left, c6top, x, y, 60 );
		mouseParallax ( 'top-par-7', c7left, c7top, x, y, 30 );
		mouseParallax ( 'top-par-8', c8left, c8top, x, y, 20 );
		mouseParallax ( 'top-par-9', c9left, c9top, x, y,28 );
		mouseParallax ( 'top-par-10', c10left, c10top, x, y, 10 );
		mouseParallax ( 'top-par-11', c11left, c11top, x, y, 10 );
		mouseParallax ( 'top-par-12', c12left, c12top, x, y, 20 );
		mouseParallax ( 'top-par-13', c13left, c13top, x, y, 24 );
		mouseParallax ( 'top-par-14', c14left, c14top, x, y, 14 );
		mouseParallax ( 'top-par-15', c15left, c15top, x, y, 29 );
	}
	
}

function mouseParallax ( id, left, top, mouseX, mouseY, speed ) {
	var obj = document.getElementById ( id );
	var parentObj = obj.parentNode,
	containerWidth = parseInt( parentObj.offsetWidth ),
	containerHeight = parseInt( parentObj.offsetHeight );
	obj.style.left = left - ( ( ( mouseX - ( parseInt( obj.offsetWidth ) / 2 + left ) ) / containerWidth ) * speed ) + 'px';
	obj.style.top = top - ( ( ( mouseY - ( parseInt( obj.offsetHeight ) / 2 + top ) ) / containerHeight ) * speed ) + 'px';
} 


function cssParallax(cont, el, radiusVal){
  $(cont).mousemove(function(event) {
      
      cx = Math.ceil($(window).width() / 2.0);
      cy = Math.ceil($(window).height() / 2.0);
    dx = event.pageX - cx;
      dy = event.pageY - cy;
      
      tiltx = (dy / cy);
      tilty = - (dx / cx);
      radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
      degree = (radius * radiusVal);

      $(el, cont).css('-webkit-transform','rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)');
      $(el, cont).css('transform','rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)');
  });
}

$(document).ready(function() {
  cssParallax('.parallax-container', '.parallax-base', 30);
  cssParallax('.parallax-container.first', '.parallax-base', 30);
});





